﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRageMath;

namespace IngameScript {


    partial class Program {


        public class Job {

            public List<Waypoint> awayPath = new List<Waypoint>();
            public List<Waypoint> homePath = new List<Waypoint>();
            public Vector3D portPositon;

            Func<Waypoint, string> waypointToString = delegate (Waypoint wp) {
                return wp.ToString();
            };

            public override string ToString() {
                string output = "Home:\n";
                output += string.Join("", homePath.Select(waypointToString));
                output += "Away:\n";
                output += string.Join("", awayPath.Select(waypointToString));
                output += "Port:\n";
                output += String.Format("{0}:\n", portPositon.GetStringComma());

                return output;
            }

        }


        public class Waypoint {
            public Vector3D Point;
            public ActionType Action;

            public enum ActionType {
                None,
                Dock,
                RequestPort
            }

            public override string ToString() {

                return String.Format("WP[A:{0};P:{1}]\n", Action.GetDescription(), Point.GetStringComma());

            }
        }


        public struct WaypointTmp {
            public MyIniKey Key;
            public WaypointType PointType;
        }

        public struct WaypointValue {
            public string Value;
            public WaypointType PointType;
        }

        public enum WaypointType {
            Home,
            Away
        }
    }
}
