﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRageMath;

namespace IngameScript {
    partial class Program {
        public interface IShipHelper {
            IMyShipConnector Connector { get; }
            bool IsConnected { get; }

            bool IsDebug { get; set; }

            bool IsAutopilotOn { get; }

            bool IsCargoFull { get; }
            bool IsCargoEmpty { get; }

            void SetAutopilot(bool isOn);

            void SetSpeedLimit(float limit);

            bool Dock();

            void ClearWaypoints();
            void AddWaypoint(Waypoint wp);
            void AddWaypoint(Vector3D point);

            List<IMyThrust> GetThrustersForDirection(Base6Directions.Direction direction);

            double GetShipSpeed();

            void Disconnect();

            void WriteDebug(string input, params object[] opts);
            void WriteDebug(bool append, string input, params object[] opts);
            void RequestDocking();

            float GetBatteryPercentage();
        }

    }
}
