﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRageMath;

namespace IngameScript {
    // This template is intended for extension classes. For most purposes you're going to want a normal
    // utility class.
    // https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/extension-methods
    static class Class1 {

        public static string GetDescription(this Program.Waypoint.ActionType actionType) {
            switch (actionType) {
                case Program.Waypoint.ActionType.Dock: return "D";
                case Program.Waypoint.ActionType.RequestPort: return "RP";
                default: return "N";
            }
        }

        public static string GetStringComma(this Vector3D vector) {
            return String.Format("{0},{1},{2}", vector.X, vector.Y, vector.Z);
        }

        public static bool Contains(this Program.JobHandler.StatusType statusType, Program.JobHandler.StatusType otherType) {
            return (statusType & otherType) == otherType;
        }

        public static void OverrideThrusters(this List<IMyThrust> thrusters, float amount) {
            thrusters.ForEach(t => t.ThrustOverride = amount);
        }

        public static void OverrideThrusterPercentage(this List<IMyThrust> thrusts, float amount) {
            thrusts.ForEach(t => t.ThrustOverridePercentage = amount);
        }

    }
}
