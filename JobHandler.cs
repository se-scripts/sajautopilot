﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRageMath;

namespace IngameScript {
    partial class Program {
        public class JobHandler {

            public Job CurrentJob;
            public StatusType JobStatus = StatusType.None;
            public IShipHelper ShipHelper;
            public RouteType CurrentRoute { get; set; } = RouteType.Outbound;
            private bool isAutoPilotReady = false;
            private int PathIndex = 0;

            public JobHandler(IShipHelper shipHelper) {
                ShipHelper = shipHelper;
            }
            

            public void StartJob(Job job) {
                CurrentJob = job;
                JobStatus = StatusType.Ready;
            }

            public enum StatusType {
                None = 0,
                Ready = 1,
                InRoute  = 1 << 1,
                Docking = 1 << 2,
                Docked = 1 << 3,
                UnDocking = 1 << 4,
                AwaitingIGC = 1 << 5,
                RequestDockingPort = 1 << 6
            }

            public enum RouteType {
                Inbound,
                Outbound
            }

            public void StartTestDocking(Job job) {
                CurrentJob = job;
                JobStatus = StatusType.Docking;
                CurrentRoute = RouteType.Outbound;
            }

            public void DockAt(Vector3D vector3D) {
                JobStatus = StatusType.InRoute;
                ShipHelper.ClearWaypoints();
                ShipHelper.AddWaypoint(vector3D);
                ShipHelper.SetAutopilot(true);
                isAutoPilotReady = true;
                
            }

            public StatusType Execute() {
                var status = JobStatus.ToString();

                ShipHelper.WriteDebug(false, "Job status: {0}", status);
                ShipHelper.WriteDebug("\tIs outbound? {0}", CurrentRoute == RouteType.Outbound);
                ShipHelper.WriteDebug("\tIs docking? {0}", JobStatus == StatusType.Docking);
                ShipHelper.WriteDebug("\tIs docked? {0}", JobStatus == StatusType.Docked);

                switch(JobStatus) {
                    case StatusType.Ready:
                        PrepairForLaunch();
                        break;
                    case StatusType.InRoute:
                        if(CurrentRoute == RouteType.Outbound) {
                            FlyOutbound();
                        }
                        else if (CurrentRoute == RouteType.Inbound) {
                            FlyInbound();
                        }
                        break;
                    case StatusType.Docking:
                        Docking();
                        break;

                    case StatusType.Docked:
                        Docked();
                        break;

                    case StatusType.UnDocking:
                        UnDocking();
                        break;

                    case StatusType.RequestDockingPort:
                        RequestDocking();
                        break;
                }

                return JobStatus;
            }

            private void PrepairForLaunch() {
                if(ShipHelper.IsConnected) {
                    ShipHelper.Disconnect();
                }
                JobStatus = StatusType.InRoute;
                CurrentRoute = RouteType.Outbound;


            }

            private void FlyOutbound() {
                if(!isAutoPilotReady) {
                    // Add away waypoints
                    ShipHelper.ClearWaypoints();
                    CurrentJob.awayPath.ForEach(wp => ShipHelper.AddWaypoint(wp));
                    ShipHelper.AddWaypoint(CurrentJob.portPositon);
                    ShipHelper.SetSpeedLimit(15.0f);
                    ShipHelper.SetAutopilot(true);
                    isAutoPilotReady = true;

                }
                else {
                    if(!ShipHelper.IsAutopilotOn) {
                        isAutoPilotReady = false;
                        JobStatus = StatusType.Docking;
                    } 
                }
            }

            private void FlyInbound() {
                if(!isAutoPilotReady) {
                    ShipHelper.ClearWaypoints();
                    CurrentJob.homePath.ForEach(wp => ShipHelper.AddWaypoint(wp));
                    ShipHelper.SetSpeedLimit(15.0f);
                    ShipHelper.SetAutopilot(true);
                    isAutoPilotReady = true;
                }
                else {
                    if(!ShipHelper.IsAutopilotOn && !IsDockingRequested) {
                        JobStatus = StatusType.RequestDockingPort;
                        isAutoPilotReady = false;
                    }
                    else if (!ShipHelper.IsAutopilotOn && IsDockingRequested) {
                        JobStatus = StatusType.Docking;
                    }
                }
            }

            private void Docking() {
                
                if(ShipHelper.Dock()) {
                    JobStatus = StatusType.Docked;
                }

            }

            private Vector3D startPoint;
            private void UnDocking() {
                if (ShipHelper.IsConnected) {
                    ShipHelper.Connector.Disconnect();
                }
                if (startPoint == Vector3D.Zero) {
                    startPoint = ShipHelper.Connector.GetPosition();
                }

                var travelLength = (ShipHelper.Connector.GetPosition() - startPoint).Length();
                var upThrusters = ShipHelper.GetThrustersForDirection(Base6Directions.Direction.Up);

                var currentPos = ShipHelper.Connector.GetPosition();
                var delta = Vector3.Distance(startPoint, currentPos);

                if (delta < 10.0f) {
                    if (ShipHelper.GetShipSpeed() > 1) {
                        upThrusters.OverrideThrusters(0.0f);
                    }
                    else {
                        upThrusters.OverrideThrusterPercentage(1.0f);
                    }
                } else {
                    upThrusters.OverrideThrusters(0.0f);
                    JobStatus = StatusType.InRoute;
                    if (CurrentRoute == RouteType.Outbound)
                        CurrentRoute = RouteType.Inbound;
                    else
                        CurrentRoute = RouteType.Outbound;

                    startPoint = Vector3D.Zero;
                }

                ShipHelper.WriteDebug("Ship trawle distance: {0}", delta);
                ShipHelper.WriteDebug("Start point: {0}", startPoint);
                ShipHelper.WriteDebug("Curre point: {0}", currentPos);




            }

            private void Docked() {
                if(CurrentRoute == RouteType.Inbound) {
                    DockedInbound();
                }
                else {
                    DockedOutbound();
                }
            }

            private void DockedInbound() {
                if (ShipHelper.IsCargoEmpty) {
                    IsDockingRequested = false;
                    isAutoPilotReady = false;
                    JobStatus = StatusType.UnDocking;
                }

            }
            private void DockedOutbound() {
                if(ShipHelper.IsCargoFull) {
                    JobStatus = StatusType.UnDocking;
                    
                }

            }

            private bool IsDockingRequested = false;
            private void RequestDocking() {
                if(!IsDockingRequested) {
                    IsDockingRequested = true;
                    ShipHelper.RequestDocking();
                    JobStatus = StatusType.AwaitingIGC;
                }

            }
        }
    }
}
