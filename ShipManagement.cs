﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRageMath;

namespace IngameScript {
    partial class Program {
        public class ShipManagement : IShipHelper {

            private readonly IMyShipConnector connector;
            private readonly IMyRemoteControl remoteControl;
            private readonly IMyTextPanel debugPanel;
            private readonly List<IMyThrust> thrusters;
            private readonly IMyGridTerminalSystem gridSystem;
            private readonly IMyIntergridCommunicationSystem IGC;

            public Dictionary<Base6Directions.Direction, List<IMyThrust>> shipThrusters = new Dictionary<Base6Directions.Direction, List<IMyThrust>>();
            

            public ShipManagement(IMyGridTerminalSystem gs, IMyIntergridCommunicationSystem igc) {
                gridSystem = gs;
                IGC = igc;

                var connector = gs.GetBlockWithName("[AUTO] Connector") as IMyShipConnector;
                Connector = connector;
                var remoteControl = gs.GetBlockWithName("*Remote Controller") as IMyRemoteControl;
                this.remoteControl = remoteControl;

                List<IMyThrust> tmpThrusters = new List<IMyThrust>();
                gs.GetBlocksOfType(tmpThrusters);
                thrusters = tmpThrusters;


                var debugPanel = GetDebugPanel(gs);
                if(debugPanel != null) {
                    IsDebug = true;
                    this.debugPanel = debugPanel;
                    WriteDebug("Debug online...");
                }


            }

            public IMyShipConnector Connector {get; }

            public bool IsConnected => Connector.Status == MyShipConnectorStatus.Connected;

            public bool IsAutopilotOn => remoteControl.IsAutoPilotEnabled;

            public bool IsDebug { get; set; } = false;

            public bool IsCargoFull { get {
                    var cube = Connector.CubeGrid;

                    List<IMyCargoContainer> containers = new List<IMyCargoContainer>();
                    gridSystem.GetBlocksOfType(containers, block => block.CubeGrid == cube);

                    bool inventoryFull = true;

                    containers.ForEach(c => {
                        if (!c.GetInventory().IsFull) inventoryFull = false;
                    });

                    return inventoryFull;
                }
            }

            public bool IsCargoEmpty { get {

                    var cube = Connector.CubeGrid;

                    List<IMyCargoContainer> containers = new List<IMyCargoContainer>();
                    gridSystem.GetBlocksOfType(containers, b => b.CubeGrid == cube);

                    bool inventoryEmpty = true;

                    containers.ForEach(c => {
                        if(c.GetInventory().ItemCount > 0) {
                            inventoryEmpty = false;
                        }
                    });

                    return inventoryEmpty;
                }
            }



            public void AddWaypoint(Waypoint wp) {
                remoteControl.AddWaypoint(wp.Point, ShipManagement.GetNextName());
            }

            public void AddWaypoint(Vector3D point) {
                remoteControl.AddWaypoint(point, "Docking port");
            }


            private static int count = 0;
            private static string GetNextName() {
                return String.Format("Path: {0}", count++);
            }

            public void ClearWaypoints() {
                remoteControl.ClearWaypoints();
            }

            public void Disconnect() {
                Connector.Disconnect();
            }

            public void SetAutopilot(bool isOn) {
                remoteControl.SetAutoPilotEnabled(isOn);
            }

            public void SetSpeedLimit(float limit) {
                remoteControl.SpeedLimit = limit;
            }

            public void WriteDebug(bool append, string input, params object[] opts) {
                if (!IsDebug) return;

                var output = String.Format(input, opts) + Environment.NewLine;
                debugPanel.WriteText(output, append);
            }
            public void WriteDebug(string input, params object[] opts) {
                WriteDebug(true, input, opts);
            }

            public bool Dock() {
                var downThrusters = GetThrustersForDirection(Base6Directions.Direction.Down);
                //var avaiableThrust = downThrusters.Select(t => t.MaxEffectiveThrust).Sum();
                //var shipMass = remoteControl.CalculateShipMass().TotalMass;
                //var gravityVector = remoteControl.GetTotalGravity();

                if (Connector.Status == MyShipConnectorStatus.Connectable) {
                    downThrusters.OverrideThrusters(0.0f);
                    Connector.Connect();
                    return true;
                }
                else if(Connector.Status == MyShipConnectorStatus.Connected) {
                    return true;
                }
                else {
                    var shipVel = remoteControl.GetShipVelocities().LinearVelocity;
                    if (shipVel.Length() > 1.0) {
                        downThrusters.OverrideThrusters(0.0f);
                    }
                    else {
                        downThrusters.OverrideThrusters(0.1f);
                    }
                    return false;
                }
            }

            public double GetShipSpeed() {
                return remoteControl.GetShipVelocities().LinearVelocity.Length();
            }

            public void RequestDocking() {
                IGC.SendBroadcastMessage("BASE", "RequestPad:1");
            }


            public List<IMyThrust> GetThrustersForDirection(Base6Directions.Direction direction) {
                if(shipThrusters.ContainsKey(direction)) {
                    return shipThrusters[direction];
                }

                var shipForward = Base6Directions.Direction.Forward;
                var shipUp = Base6Directions.Direction.Up;
                var shipOrientation = new MyBlockOrientation(shipForward, shipUp);

                List<IMyThrust> tmpThrusters = new List<IMyThrust>();

                thrusters.ForEach(thruster => {
                    var facing = thruster.Orientation.TransformDirection(shipForward);
                    var thrustFacing = Base6Directions.GetFlippedDirection(facing);
                    var shipThrustDirection = shipOrientation.TransformDirectionInverse(thrustFacing);

                    if (shipThrustDirection == direction) tmpThrusters.Add(thruster);

                });

                shipThrusters.Add(direction, tmpThrusters);
                return tmpThrusters;
            }

            private IMyTextPanel GetDebugPanel(IMyGridTerminalSystem gs) {
                List<IMyTextPanel> textPanels = new List<IMyTextPanel>();
                gs.GetBlocksOfType(textPanels);

                var panel = textPanels.Find(p => p.CustomName == "[AUTO] Debug");
                return panel;

            }

            public float GetBatteryPercentage() {
                
            }
        }
    }
}
