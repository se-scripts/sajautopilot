﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRageMath;

namespace IngameScript {
    partial class Program : MyGridProgram {

        const string UNICAST_CALLBACK = "DIRECT_MSG";


        MyIni myIni = new MyIni();

        List<Vector3D> _pathVectors = new List<Vector3D>();
        IMyRemoteControl remoteControl;
        IMyShipConnector connector;

        ShipManagement ShipManager;
        JobHandler JobManager;

 
        Job CurrentJob = new Job();
        public Program() {
            MyIniParseResult initResult;
            var result = myIni.TryParse(Me.CustomData, out initResult);
            if (!result) return;


            ShipManager = new ShipManagement(GridTerminalSystem, IGC);
            JobManager = new JobHandler(ShipManager);


            Runtime.UpdateFrequency = UpdateFrequency.Update10;
            


            remoteControl = GridTerminalSystem.GetBlockWithName("*Remote Controller") as IMyRemoteControl;
            connector = GridTerminalSystem.GetBlockWithName("Connector") as IMyShipConnector;


            IGC.UnicastListener.SetMessageCallback(UNICAST_CALLBACK);

        }

        public void Save() {

        }

        public void Main(string argument, UpdateType updateSource) {

            UpdateType type = (updateSource & UpdateType.Terminal | UpdateType.Trigger);

            bool terminalButton =
                (updateSource & UpdateType.Terminal) == UpdateType.Terminal ||
                (updateSource & UpdateType.Trigger) == UpdateType.Trigger;


            if (terminalButton) {
                if (argument == "1") {
                    IGC.SendBroadcastMessage("BASE", "RequestJob:MyFirstPath", TransmissionDistance.AntennaRelay);
                }
                else if (argument == "dock") {
                    JobManager.StartTestDocking(LoadJobFromConfig(myIni));
                }
                else if (argument == "req home") {
                    var remPos = remoteControl.GetPosition();
                    IGC.SendBroadcastMessage("BASE", "ReqPath:" + String.Format("{0},{1},{2},H", remPos.X, remPos.Y, remPos.Z));
                }
                else if (argument == "req away") {
                    var remPos = remoteControl.GetPosition();
                    IGC.SendBroadcastMessage("BASE", "ReqPath:" + String.Format("{0},{1},{2},A", remPos.X, remPos.Y, remPos.Z));
                }
                else if (argument == "req port") {
                    IMyShipConnector shipConnector = GridTerminalSystem.GetBlockWithName("[AUTO] Connector") as IMyShipConnector;
                    if (shipConnector.Status != MyShipConnectorStatus.Connected) { return; }

                    IMyShipConnector otherConnector = shipConnector.OtherConnector;
                    Vector3D otherConnectorPosition = otherConnector.GetPosition();
                    int safeDistance = 10;

                    otherConnectorPosition = (otherConnector.WorldMatrix.Forward * safeDistance) + otherConnectorPosition;
                    IGC.SendBroadcastMessage("BASE", "RegisterPad:" + String.Format("{0},{1},{2}", otherConnectorPosition.X, otherConnectorPosition.Y, otherConnectorPosition.Z));
                }
                else if (argument.StartsWith("StartJob:")) {
                    var jobName = argument.Replace("StartJob:", "");
                    IGC.SendBroadcastMessage("BASE", String.Format("RequestJob:{0}", jobName), TransmissionDistance.AntennaRelay);
                }
                else {
                    


                }
            }
            else {
                HandleUnicastMessage();
            }
        }

        private void HandleUnicastMessage() {
            Echo("Got Unicast message");
            

            var unicastMessae = IGC.UnicastListener.AcceptMessage();
            Echo("Tag: " + unicastMessae.Tag);

            switch (unicastMessae.Tag) {

                case "PortUpperPoint": {
                        JobManager.DockAt(unicastMessae.As<Vector3D>());
                        break;
                    }

                case "NewJob": {

                        var response = unicastMessae.As<string>();
                        response = Encoding.UTF8.GetString(Convert.FromBase64String(response));

                        MyIni jobConfig = new MyIni();

                        MyIniParseResult parseResult;
                        if(!jobConfig.TryParse(response, out parseResult)) {
                            Echo("Error while parsing config: " + parseResult.ToString());
                        }

                        Me.CustomData = response;
                        Echo("New Job");

                        Job job = LoadJobFromConfig(jobConfig);
                        StartJob(job);

                        break;
                    }

                case "HomePath": {
                        Waypoint wp = CreateWaypoint(unicastMessae.As<string>());
                        CurrentJob.homePath.Add(wp);
                        break;
                    }
                case "AwayPath": {
                        Waypoint wp = CreateWaypoint(unicastMessae.As<string>());
                        CurrentJob.awayPath.Add(wp);
                        break;
                    }
                case "JobPort": {
                        CurrentJob.portPositon = unicastMessae.As<Vector3D>();
                        break;
                    }
                case "JobDone": {
                        Echo("Job Done");
                        break;
                    }

            }

            var jobStatus = JobManager.Execute();
            if (jobStatus == JobHandler.StatusType.Docking) {
                Runtime.UpdateFrequency = UpdateFrequency.Update1;
            } else {
                Runtime.UpdateFrequency = UpdateFrequency.Update10;
            }
        }

        private Waypoint CreateWaypoint(string input) {
            var components = input.Split(new char[] { ',' });
            Waypoint wp = new Waypoint();
            Vector3D vector = new Vector3D {
                X = double.Parse(components[0]),
                Y = double.Parse(components[1]),
                Z = double.Parse(components[2])
            };

            switch (components[3]) {
                case "D":
                    wp.Action = Waypoint.ActionType.Dock;
                    break;
                case "RP":
                    wp.Action = Waypoint.ActionType.RequestPort;
                    break;
                default:
                    wp.Action = Waypoint.ActionType.None;
                    break;
            }

            wp.Point = vector;

            return wp;
        }

        private void StartJob(Job job) {

            JobManager.StartJob(job);


        }

        private Job LoadJobFromConfig(MyIni config) {
            if(!config.ContainsSection("job")) {
                throw new Exception("Job config missing");
            }

            List<MyIniKey> tmpKeys = new List<MyIniKey>();
            config.GetKeys("job", tmpKeys);

            Func<MyIniKey, WaypointType, WaypointTmp> func = delegate (MyIniKey key, WaypointType waypointType) {
                return new WaypointTmp() {
                    Key = key,
                    PointType = waypointType
                };
            };

            var pad = config.Get("job", "pad");
            var homePointKeys = tmpKeys.FindAll(k => k.Name.StartsWith("hp")).Select(k => func(k, WaypointType.Home)).ToList();
            var awayPointKeys = tmpKeys.FindAll(k => k.Name.StartsWith("ap")).Select(k => func(k, WaypointType.Away)).ToList();
            List<WaypointTmp> waypointTmps = new List<WaypointTmp>();
            waypointTmps.AddList(homePointKeys);
            waypointTmps.AddList(awayPointKeys);


            Job job = new Job();

            Action<WaypointTmp> waypointMapper = delegate (WaypointTmp waypointTmp) {
                MyIniValue value = config.Get(waypointTmp.Key);
                string valueStr = value.ToString();

                List<KeyValuePair<string, string>> v = valueStr.Split(new char[] { ';' }).Select(st => {
                    var pair = st.Split(new char[] { ':' });
                    return new KeyValuePair<string, string>(pair[0], pair[1]);
                }).ToList();

                Waypoint wp = new Waypoint();

                v.ForEach(pair => {
                    switch(pair.Key) {
                        case "A": // Action
                            wp.Action = CreateActionType(pair.Value);
                            break;
                        case "P": // Point
                            wp.Point = CreateVectorFromString(pair.Value);
                            break;
                    }
                });
                
                switch(waypointTmp.PointType) {
                    case WaypointType.Away:
                        job.awayPath.Add(wp);
                        break;
                    case WaypointType.Home:
                        job.homePath.Add(wp);
                        break;
                }

            };

            waypointTmps.ForEach(waypointMapper);

            job.portPositon = CreateVectorFromString(pad.ToString());




            return job;
        }

        private Waypoint.ActionType CreateActionType(string input) {
            Waypoint.ActionType actionType;
            if (input == "D") { // Dock
                actionType = Waypoint.ActionType.Dock;
            }
            else if (input == "RP") {
                actionType = Waypoint.ActionType.RequestPort;
            }
            else {
                actionType = Waypoint.ActionType.None;
            }

            return actionType;
        }

        private Vector3D CreateVectorFromString(string input) {
            string[] components = input.Split(new char[] { ',' });
            return new Vector3D() {
                X = double.Parse(components[0]),
                Y = double.Parse(components[1]),
                Z = double.Parse(components[2]),
            };
        }

    }
}